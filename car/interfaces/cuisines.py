from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class CuisineType:
    uuid: str
    label: str
    short: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class Cuisine:
    uuid: str  # UUID
    dish_name: str
    couisine_type: CuisineType  # puede llevar uuid o id de cuisine_type
    price: int
    created_at: datetime
    modified_at: datetime

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_cuisine_type(self):
        return self.cuisine_type.label

    @property
    def get_price(self):
        return self.price
