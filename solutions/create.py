from bson.objectid import ObjectId
from databases.client import MongoLsv  # noqa




cuisineLP_lsv = MongoLsv()
print("--------------------------------")
print(
    "Data Bases"
)
print("")
print(cuisineLP_lsv.list_dbs())
print("--------------------------------")
print(
    "Collections"
)
print("")
print(cuisineLP_lsv.list_collections(db_name="cuisines_db"))
print("--------------------------------")
print(
    "Cuisine Types"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisine_type"
    )
)
print("--------------------------------")
print(
    "Cuisines"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisines"
    )
)




#CREATE METHOD
#Collection: Cousine Types

""" print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record={"label": "Italian Cuisine", "short": "IC"},
    )
) 



print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record={"label": "Spanish Cuisine", "short": "SC"},
    )
) 



print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record={"label": "French Cuisine", "short": "FC"},
    )
) """

## Collection : Cousines

""" print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Insalata di riso", "cousine_type": "62ad653bd3bded1c89109d72","price":35400,"created_at":"2022-06-12","modified_at":"2022-06-12"},
    )
)

print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Risotto alla zucca", "cousine_type": "62ad653bd3bded1c89109d72","price":47000,"created_at":"2022-06-18","modified_at":"2022-06-18"},
    )
)

print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Croque monsieur", "cousine_type": "62ad653bd3bded1c89109d74","price":25000,"created_at":"2022-06-19","modified_at":"2022-06-21"},
    )
)

print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Blanquette de veau", "cousine_type": "62ad653bd3bded1c89109d74","price":12500,"created_at":"2022-06-15","modified_at":"2022-06-19"},
    )
)

print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Callos a la madrileña", "cousine_type": "62ad653bd3bded1c89109d73","price":81000,"created_at":"2022-06-10","modified_at":"2022-06-12"},
    )
)

print(
    cuisineLP_lsv.create_new_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record={"dish_name": "Gazpacho manchego", "cousine_type": "62ad653bd3bded1c89109d73","price":71000,"created_at":"2022-06-17","modified_at":"2022-06-18"},
    )
) """

