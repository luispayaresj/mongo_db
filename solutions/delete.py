from bson.objectid import ObjectId  # noqa

from databases.client import MongoLsv

cuisineLP_lsv = MongoLsv()
print("--------------------------------")
print(
    "Data Bases"
)
print("")
print(cuisineLP_lsv.list_dbs())
print("--------------------------------")
print(
    "Collections"
)
print("")
print(cuisineLP_lsv.list_collections(db_name="cuisines_db"))
print("--------------------------------")
print(
    "Cuisine Types"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisine_type"
    )
)
print("--------------------------------")
print(
    "Cuisines"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisines"
    )
)


#DELETE METHOD

#Collection: Cousine Types

""" print(
    cuisineLP_lsv.delete_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record_id="62ad653bd3bded1c89109d74",
    )
) """

#Collection: Cousines

""" print(
    cuisineLP_lsv.delete_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record_id="62ad68738105809aaea1417e",
    )
)

print(
    cuisineLP_lsv.delete_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record_id="62ad68738105809aaea1417f",
    )
) """
