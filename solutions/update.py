from bson.objectid import ObjectId  # noqa

from databases.client import MongoLsv

cuisineLP_lsv = MongoLsv()
print("--------------------------------")
print(
    "Data Bases"
)
print("")
print(cuisineLP_lsv.list_dbs())
print("--------------------------------")
print(
    "Collections"
)
print("")
print(cuisineLP_lsv.list_collections(db_name="cuisines_db"))
print("--------------------------------")
print(
    "Cuisine Types"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisine_type"
    )
)
print("--------------------------------")
print(
    "Cuisines"
)
print("")
print(
    cuisineLP_lsv.get_records_from_collection(
        db_name="cuisines_db", collection="cuisines"
    )
)

#UPDATE METHOD

#Collection: Cousine Types

""" print(
    cuisineLP_lsv.update_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record_query={"_id": ObjectId("62ad653bd3bded1c89109d72")},
        record_new_value={"short": "CI"},
    )
) 

print(
    cuisineLP_lsv.update_record_in_collection(
        db_name="cuisines_db",
        collection="cuisine_type",
        record_query={"_id": ObjectId("62ad653bd3bded1c89109d72")},
        record_new_value={"label": "Cocina Italiana"},
    )
) """

#Collection: Cousines

""" print(
    cuisineLP_lsv.update_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record_query={"_id": ObjectId("62abee7842881c1f6f99005b")},
        record_new_value={"dish_name": "Catalina Marcela"},
    )
)
"""

""" print(
    cuisineLP_lsv.update_record_in_collection(
        db_name="cuisines_db",
        collection="cuisines",
        record_query={"_id": ObjectId("62abee7842881c1f6f99005b")},
        record_new_value={"price": 32000},
    )
)
"""