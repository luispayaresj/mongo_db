from bson.objectid import ObjectId  # noqa

from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print("--------------------------------")
print(
    "Data Bases"
)
print("")
print(mongo_lsv.list_dbs())
print("--------------------------------")
print(
    "Collections"
)
print("")
print(mongo_lsv.list_collections(db_name="car_lsv"))
print("--------------------------------")
print(
    "Documents Types"
)
print("")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="documents_types"
    )
)
print("--------------------------------")
print(
    "Students"
)
print("")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="students"
    )
)
""" print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_query={"_id": ObjectId("62a93d7407f4b95ac517f990")},
        record_new_value={"label": "Número de Identificación Tributario"},
    )
) """

""" print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record_query={"_id": ObjectId("62abee7842881c1f6f99005b")},
        record_new_value={"first_name": "Catalina Marcela"},
    )
)
"""

""" print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_query={"_id": ObjectId("62a93d7407f4b95ac517f990")},
        record_new_value={"label": "Pasaporte"},
    )
) """

#DELETE METHOD
""" print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_id="62a93eecc33315d423281ff1",
    )
) """


""" print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record_id="62abee7842881c1f6f99005c",
    )
) """

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record={"label": "Numero de identificacion Tributario", "value": "NIT"},
    )
) """

#CREATE METHOD

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Juan", "last_name": "Mejia","document_number":"123456", "documents_types": ObjectId("62a93d5207f4b95ac517f98e"),"created_at":"2022-06-12","modified_at":"2022-06-12"},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Oriana", "last_name": "Tamara","document_number":"4455668877", "documents_types": ObjectId("62a93d5207f4b95ac517f98e"),"created_at":"2022-06-14","modified_at":"2022-06-14"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Kevin", "last_name": "Jaraba","document_number":"78945611", "documents_types": ObjectId("62a93d6207f4b95ac517f98f"),"created_at":"2022-06-17","modified_at":"2022-06-17"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Beatriz", "last_name": "Fernandez","document_number":"44887115", "documents_types": ObjectId("62a93d6207f4b95ac517f98f"),"created_at":"2022-06-15","modified_at":"2022-06-15"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Catalina", "last_name": "Hans","document_number":"987411523", "documents_types": ObjectId("62a93d7407f4b95ac517f990"),"created_at":"2022-06-12","modified_at":"2022-06-12"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="students",
        record={"first_name": "Victor", "last_name": "Palacio","document_number":"81147335", "documents_types": ObjectId("62a93d7407f4b95ac517f990"),"created_at":"2022-06-10","modified_at":"2022-06-10"},
    )
)  """